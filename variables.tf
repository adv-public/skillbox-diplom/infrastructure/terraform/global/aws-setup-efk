locals {
  environment       = "stage"
  name_instance     = "efk"

  # Network
  availability_zone             = data.terraform_remote_state.vpc-stage.outputs.availability_zones
  vpc_id                        = data.terraform_remote_state.vpc-stage.outputs.vpc_id-stage
  subnet_id                     = data.terraform_remote_state.vpc-stage.outputs.subnet_id-stage
  vpc_default_security_group_id = data.terraform_remote_state.vpc-stage.outputs.vpc_default_security_group_id-stage

  allowed_ip_list               = compact(concat(["5.9.118.242"], data.terraform_remote_state.instance-ip-prod.outputs.instance_ips, data.terraform_remote_state.instance-ip-stage.outputs.instance_ips))
  
}

#Module      : SUBNET
#Description : Terraform SUBNET module variables.

variable "availability_zones" {
  type        = list(string)
  default     = []
  description = "List of Availability Zones (e.g. `['us-east-1a', 'us-east-1b', 'us-east-1c']`)."
}

variable "elasticsearch_in_port" {
  description   = "The public port the elasticsearch will use for HTTP requests"
  type          = number
  default       = 9200
}

variable "kibana_in_port" {
  description   = "The public port the kibana will use for HTTP requests"
  type          = number
  default       = 5601
}

variable "instance_security_group_name" {
  description   = "The name of the security group for the EC2 Instances"
  type          = string
  default       = "efk-instance"
}

# Module      : terraform-aws-elasticsearch 
# Description : Terraform elasticsearch module variables.
variable "instance_type" {
  type        = string
  description = "The type of the instance"
  default     = "t3.small.elasticsearch"
}

variable "elasticsearch_version" {
  type        = string
  description = "Version of Elasticsearch to deploy (_e.g._ '7.10', `7.1`, `6.8`, `6.7`, `6.5`, `6.4`, `6.3`, `6.2`, `6.0`, `5.6`, `5.5`, `5.3`, `5.1`, `2.3`, `1.5`"
  default     = "7.10"
}

variable "instance_count" {
  type        = number
  description = "Number of data nodes in the cluster"
  default     = 2
}

variable "zone_awareness_enabled" {
  type        = bool
  description = "Enable zone awareness for Elasticsearch cluster"
  default     = true
}

variable "encrypt_at_rest_enabled" {
  type        = bool
  description = "Whether to enable encryption at rest"
  default     = true
}

variable "vpc_enabled" {
  type        = bool
  description = "Set to false if ES should be deployed outside of VPC."
  default     = false
}


variable "dedicated_master_enabled" {
  type        = bool
  description = "Indicates whether dedicated master nodes are enabled for the cluster"
  default     = false
}

variable "elasticsearch_subdomain_name" {
  type        = string
  description = "The name of the subdomain for Elasticsearch in the DNS zone (_e.g._ `elasticsearch`, `ui`, `ui-es`, `search-ui`)"
  default     = "elasticsearch"
}

variable "kibana_subdomain_name" {
  type        = string
  description = "The name of the subdomain for Kibana in the DNS zone (_e.g._ `kibana`, `ui`, `ui-es`, `search-ui`, `kibana.elasticsearch`)"
  default     = "kibana"
}

variable "create_iam_service_linked_role" {
  type        = bool
  description = "Whether to create `AWSServiceRoleForAmazonElasticsearchService` service-linked role. Set it to `false` if you already have an ElasticSearch cluster created in the AWS account and AWSServiceRoleForAmazonElasticsearchService already exists. See https://github.com/terraform-providers/terraform-provider-aws/issues/5218 for more info"
  default     = true
}

variable "ebs_volume_size" {
  type        = number
  description = "EBS volumes for data storage in GB"
  default     = 10
}

variable "dns_zone_id" {
  type        = string
  description = "Route53 DNS Zone ID to add hostname records for Elasticsearch domain and Kibana"
  default     = "Z08927722L9MXOLJYMK0R"
}

variable "domain_hostname_enabled" {
  type        = bool
  description = "Explicit flag to enable creating a DNS hostname for ES. If `true`, then `var.dns_zone_id` is required."
  default     = true
}

variable "kibana_hostname_enabled" {
  type        = bool
  description = "Explicit flag to enable creating a DNS hostname for Kibana. If `true`, then `var.dns_zone_id` is required."
  default     = true
}

variable "allowed_cidr_blocks" {
  type        = list(string)
  default     = []
  description = "List of CIDR blocks to be allowed to connect to the cluster"
}