## Network
data "terraform_remote_state" "vpc-stage" {
  backend = "local"
  
  config = {
    path = "${path.module}/../../global/aws-vpc/state/terraform.tfstate"

   }
}

data "terraform_remote_state" "instance-ip-stage" {
  backend = "local"
  
  config = {
    path = "${path.module}/../../stage/aws-web-cluster-stage/services/webserver-cluster/state/terraform.tfstate"

   }
}

data "terraform_remote_state" "instance-ip-prod" {
  backend = "local"
  
  config = {
    path = "${path.module}/../../prod/aws-web-cluster-prod/services/webserver-cluster/state/terraform.tfstate"

   }
}

## Security group
# resource "aws_security_group" "elasticsearch" {
#   name        = "${local.environment}-${var.instance_security_group_name}"
#   description = "Allow Public Access"
#   vpc_id      = local.vpc_id

#   # Let's set a rule on which ports we can access our servers
#   egress = [ 
#     {
#       cidr_blocks      = [ "0.0.0.0/0" ]
#       description      = "Allow all outbound requests"
#       from_port        = 0
#       protocol         = "-1"
#       self             = false
#       to_port          = 0
#       ipv6_cidr_blocks = []
#       prefix_list_ids  = []
#       security_groups  = []
#     } 
#   ]

#   tags = merge(
#     module.this.tags,
#     {
#       "Name" = format("%s%s%s", local.name_instance, module.this.delimiter, local.environment)
#       #"AZ"   = local.availability_zone
#     }
#   )
# }

# resource "aws_security_group_rule" "http_in_elasticsearch" {
#   description       = "Allow inbound HTTP requests"
#   type              = "ingress"
#   from_port         = var.elasticsearch_in_port
#   to_port           = var.elasticsearch_in_port
#   protocol          = "tcp"
#   cidr_blocks       = ["0.0.0.0/0"]
#   security_group_id = aws_security_group.elasticsearch.id
# }

# resource "aws_security_group_rule" "http_in_kibana" {
#   description       = "Allow inbound HTTP requests"
#   type              = "ingress"
#   from_port         = var.kibana_in_port
#   to_port           = var.kibana_in_port
#   protocol          = "tcp"
#   cidr_blocks       = ["0.0.0.0/0"]
#   security_group_id = aws_security_group.elasticsearch.id
# }
