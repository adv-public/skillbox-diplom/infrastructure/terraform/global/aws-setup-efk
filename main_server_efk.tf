terraform {
  required_version = ">= 1.0.0, < 2.0.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

## Configure the AWS Provider
provider "aws" {
  region = "eu-central-1"
}

# Find out what data centers are in the selected region
# data "aws_availability_zones" "available" {}


# Elasticsearch
module "elasticsearch" {
  # source
  # source     = "git@gitlab.com:adv-public/skillbox-diplom/infrastructure/terraform/modules.git//terraform-aws-ec2?ref=main"
  source = "../../../modules/terraform-aws-elasticsearch"
  # "es:ESHttpGet", "es:ESHttpPut", "es:ESHttpPost", 
  iam_role_arns                  = ["arn:aws:iam::953557954524:user/user_kibana"]
  iam_actions                    = ["es:ESHttpGet", "es:ESHttpPut", "es:ESHttpPost", "es:*"]
  allowed_cidr_blocks            = local.allowed_ip_list

  security_groups                = [local.vpc_default_security_group_id]
  vpc_id                         = local.vpc_id
  vpc_enabled                    = var.vpc_enabled # false
  subnet_ids                     = tolist(local.subnet_id)
  zone_awareness_enabled         = var.zone_awareness_enabled
  elasticsearch_version          = var.elasticsearch_version
  instance_type                  = var.instance_type
  instance_count                 = var.instance_count
  encrypt_at_rest_enabled        = var.encrypt_at_rest_enabled
  dedicated_master_enabled       = var.dedicated_master_enabled
  create_iam_service_linked_role = var.create_iam_service_linked_role
  kibana_subdomain_name          = var.kibana_subdomain_name
  ebs_volume_size                = var.ebs_volume_size
  dns_zone_id                    = var.dns_zone_id
  kibana_hostname_enabled        = var.kibana_hostname_enabled
  domain_hostname_enabled        = var.domain_hostname_enabled

  advanced_options = {
    "rest.action.multi.allow_explicit_index" = "true"
  }

  context = module.this.context

}
